<?php
namespace AdapterPattern;

class Car
{
    private $name;
    private $brand;

    function __construct($name, $brand)
    {
        $this->name = $name;
        $this->brand = $brand;
    }

    function getName(){
        return $this->name;
    }

    function getBrand(){
        return $this->brand;
    }
}