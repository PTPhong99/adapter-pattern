<?php
use AdapterPattern\Car;

class CarAdapter
{
    private $car;

    function __construct(Car $car){
        $this->car = $car;
    }

    function getNameAndBrand(){
        return $this->car->getName() . ' of ' . $this->car->getBrand();
    }
}